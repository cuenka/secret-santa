<?php


namespace App\Services;

use App\Model\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;

class Cookies
{
    private const COOKIE_NAME = 'amigo_invisible_app_player';
    private const COOKIE_DURATION = 99999;

    public function set(Request $request, Player $player)
    {
        $games = [];

        if ($request->cookies->get(self::COOKIE_NAME) === null) {
            Cookie::queue(self::COOKIE_NAME, $this->create($player), self::COOKIE_DURATION);
        }
    }

    public function get(Request $request)
    {
        if ($request->cookies->get(self::COOKIE_NAME) === null) {
            // REDIRECT TO PLAYER NOT FOUND ERROR PAGE
        }

        $cookie = $request->cookies->get(self::COOKIE_NAME);
        $cookie = unserialize($cookie);

        if ($this->isValid($cookie) === false) {
            // REDIRECT TO PLAYER NOT FOUND ERROR PAGE
        }

        return Player::where('id', $cookie['id'])
            ->where('room_id', $cookie['room_id'])
            ->where('token', $cookie['token'])
            ->first();
    }

    private function create(Player $player)
    {
        $game =  [
            'id' => $player->getId(),
            'room_id' => $player->getRoomId(),
            'token' => $player->getToken()
        ];

        return serialize($game);
    }

    private function isValid($player)
    {
        $query = Player::where('id', $player['id'])
            ->where('room_id', $player['room_id'])
            ->where('token', $player['token']);

        if ($query->count() === 1) {
            return true;
        }
        return false;
    }
}
