<?php


namespace App\Services;


class Shuffle
{
    public static function mixUp($players)
    {
        $isMixed = false;
        $original = $players;
        while ($isMixed === false) {
            $isMixed = true;
            shuffle($players);
            reset($original);
            $assignments = null;
            foreach ($players as $player) {
                if ($player === current($original)) {
                    $isMixed = false;
                } else {
                    $assignments[] =
                        [
                            'player' => $player,
                            'buy_to' => current($original),
                        ];
                }
                next($original);
            }
        }
        return $assignments;
    }
}
