<?php


namespace App\Services;


use App\Model\Room;
use Illuminate\Support\Facades\DB;

class GameNameGenerator
{
    const code1 = ['blue', 'red', 'yellow', 'black', 'white', 'green', 'grey', 'purple', 'violet', 'orange'];
    const code2 = ['cat', 'dog', 'parrot', 'wolf', 'bird', 'bear', 'snake', 'ant', 'squirrel', 'koala'];

    public static function getName()
    {
        $isValid = false;

        while ($isValid === false) {
            $candidate = self::code1[array_rand(self::code1)] . ' ' . self::code2[array_rand(self::code2)];

            $matches = DB::table(Room::TABLE_NAME)
                ->where('code_game', $candidate)->count();
            if ($matches === 0) {
                $isValid = true;
            }
        }

        return $candidate;
    }
}
