<?php

namespace App\Http\Middleware;

use App\Services\Cookies;
use Closure;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Crypt;

class CheckCookies
{
    private $cookieService;

    /**
     * CheckCookies constructor.
     * @param $cookieService
     */
    public function __construct()
    {
        $this->cookieService = new Cookies();
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cookie = $this->cookieService->get($request);
        return $next($request);
    }
}
