<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoomResource;
use App\Model\Player;
use App\Model\Room;
use App\Rules\CodeGameValid;
use App\Rules\PlayerIsUnique;
use App\Services\Cookies;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class PlayerController extends Controller
{
    public function show(Request $request, Room $room, Cookies $cookies)
    {
        $player = $cookies->get($request);
        if ($player === null) {
            throw new \Exception('PLayer not found');
        }

        if ($request->ajax()) {
        } else {
            return view('player/show',
                [
                    'player' => $player
                ]);
        }
    }
}
