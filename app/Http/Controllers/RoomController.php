<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoomResource;
use App\Model\Player;
use App\Model\Room;
use App\Rules\CodeGameValid;
use App\Rules\PlayerIsUnique;
use App\Services\Shuffle;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class RoomController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return new RoomResource([]);
        } else {
            return view('room/index');

        }
    }

    public function create(Request $request)
    {
        if ($request->ajax()) {
            return new RoomResource([]);
        } else {
            return view('room/create',
            [
                'room' => new room(),
                'type' => $request->get('type', 'email')
            ]);
        }
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            return new RoomResource([]);
        } else {
            $request->validate(Room::getValidation($request->get('type', 'email')));
            $room = new Room();
            $room->createRoomFromRequest($request);
            $room->save();
            return redirect()->route('room.show', ['room' => $room->id]);
        }
    }

    public function show(Request $request, Room $room)
    {
        if ($request->ajax()) {
            return new RoomResource($room);
        } else {
            $entryUrl = route('room.entry', ['id' => $room->getId(),'token' => $room->getToken()]);
            $players = Player::all()->where('room_id', $room->id)->where('status', Player::GUEST_STATUS_ACTIVE);
            $assignments  = Shuffle::mixUp($players->toArray());

            return view('room/show',
                [
                    'room' => $room,
                    'players' => $players,
                    'assignments' => $assignments,
                    'entryUrl' => $entryUrl
            ]);
        }
    }

    public function edit(Request $request)
    {
        return new RoomResource([]);
    }

    public function update(Request $request)
    {
        return new RoomResource([]);
    }

    public function destroy(Request $request)
    {
        return new RoomResource([]);
    }

    public function entry(Request $request, int $id,string $token)
    {
        if ($request->ajax()) {
            return new RoomResource([]);
        } else {


            return view('room/entry',
                [
                    'id' => $id,
                    'token' => $token,
                    'room' => new Room(),
                ]);
        }
    }

    public function checkEntry(Request $request)
    {
        $request->validate([
            'id' => 'required|integer',
            'token' => 'required|alpha_num|max:60',
            'name' => ['required','string','max:255', new PlayerIsUnique($request->get('id'))],
            'code_game' => ['required', new CodeGameValid($request->get('id'))]
        ]);

        $player = new Player();
        $player->createFromRequest($request);
        if ($request->ajax()) {
        } else {
            return redirect()->route('player.show');
        }
    }


}
