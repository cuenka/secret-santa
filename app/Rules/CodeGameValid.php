<?php

namespace App\Rules;

use App\Model\Room;
use Illuminate\Contracts\Validation\Rule;

class CodeGameValid implements Rule
{
    protected $room;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $roomId)
    {
        $this->room = $roomId;
    }

    protected function getRoomId() :int
    {
        return $this->room;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) :bool
    {
        return (Room::find($this->getRoomId())->where('code_game', $value)->count() === 1);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Password does not look correct, please try again or request a link from game organiser';
    }
}
