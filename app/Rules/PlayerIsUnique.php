<?php

namespace App\Rules;

use App\Model\Player;
use Illuminate\Contracts\Validation\Rule;

class PlayerIsUnique implements Rule
{
    protected $room;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $roomId)
    {
        $this->room = $roomId;
    }

    protected function getRoomId() :int
    {
        return $this->room;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (Player::all()->where('room_id', $this->getRoomId())->where('name', $value)->count() === 0);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'That name has been already taken, and it would be very confusing this game with 2 people with the same nme, if you think this is a mistake ask the organiser of the game';
    }
}
