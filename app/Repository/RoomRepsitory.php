<?php

namespace App\Repository;

use App\Model\Room;

class RoomRepsitory
{
    public function isValidEntryRoom(int $id, string $token) :bool
    {
        if (Room::where('id', $id)->where('token', $token)->first()->count() === 1) {
            return true;
        }
        return false;
    }

    public function findByRoomCode(string $roomCode, string $token = null)
    {
        return Room::all();
    }
}
