<?php

namespace App\Model;

use App\Services\GameNameGenerator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Room extends Model
{
    public const TABLE_NAME = 'rooms';
    private const TOKEN_LENGTH = 60;

    public const ROOM_STATUS_ACTIVE = 'active';
    public const ROOM_STATUS_CANCEL = 'cancel';
    protected $table = self::TABLE_NAME;

    protected $casts = [
        'name' => 'string',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function leader()
    {
        return $this->hasOne(Player::class);
    }

    public static function getValidation(string $type) :array
    {
        $validation = [
            'name'              => 'required|string',
            'email_leader'      => 'required|e-mail',
            'name_leader'       => 'required|string',
            'leader_is_playing' => 'required|boolean',
            'budget'            => 'required|numeric',
            'date_of_game'      => 'required|date'
        ];

        if ($type === 'email') {
            $validation[] = [
                'player'        => 'array'
            ];
        }
        return $validation;
    }

    public function createRoomFromRequest(Request $request)
    {
        $type = $request->get('type', 'email');
        $this->name = $request->input('name', $this->name);
        $this->code_game = GameNameGenerator::getName();
        $this->status = self::ROOM_STATUS_ACTIVE;
        $this->date_of_game = Carbon::parse($request->input('date_of_game'));
        $this->leader_is_playing = $request->input('leader_is_playing');
        $this->budget = $request->input('budget');
        $this->token = Str::random(self::TOKEN_LENGTH);
        $this->save();
        $leader = new Player();
        $leader->name = $request->input('name_leader');
        if ($type === 'email')  $leader->email = $request->input('email_leader');
        $leader->is_playing =  $request->input('leader_is_playing');
        $leader->status = Player::GUEST_STATUS_ACTIVE;
        $leader->setRoomId($this->getId());
        $leader->token = Str::random(self::TOKEN_LENGTH);
        $leader->save();
        $this->leader = $leader->getId();
        $this->save();
        if ($type === 'email') {
            $players = $request->get('player', []);
            foreach ($players as $playerRequest) {
                $player = new Player();
                $player->status = Player::GUEST_STATUS_ACTIVE;
                $player->token = Str::random(self::TOKEN_LENGTH);
                $player->name = Arr::get($playerRequest, 'name');
                $player->email = Arr::get($playerRequest, 'email');
                $player->is_playing = true;
                $player->setRoomId($this->getId());
                $player->save();
            }
        }

    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getToken() : string
    {
        return $this->token;
    }

    public function getLeaderId() : int
    {
        return $this->leader;
    }
}
