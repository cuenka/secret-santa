<?php

namespace App\Model;

use App\Services\Cookies;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class Player extends Model
{
    const TABLE_NAME = 'players';
    private const TOKEN_LENGTH = 60;

    const GUEST_STATUS_ACTIVE = 'active';
    const GUEST_STATUS_CANCEL = 'cancel';
    protected $table = self::TABLE_NAME;

    protected $casts = [
        'name' => 'string',
        'is_playing' => 'boolean',
    ];


    public function getId() :int
    {
        return $this->id;
    }

    public function setRoomId(int $roomId)
    {
        $this->room_id = $roomId;
    }

    public function getRoomId() :int
    {
        return $this->room_id;
    }

    public function getToken() :string
    {
        return $this->token;
    }

    public function createFromRequest(Request $request)
    {
        $cookieService = new Cookies();
        $this->name = $request->input('name');
        $this->is_playing = $request->input('is_playing', true);
        $this->status = self::GUEST_STATUS_ACTIVE;
        $this->room_id = $request->input('room_id');
        $this->token = Str::random(self::TOKEN_LENGTH);

        $this->save();
        $cookieService->set($request, $this);
    }
}
