<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Room extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('token', 60);
            $table->date('date_of_game');
            $table->string('name');
            $table->string('code_game')->unique();
            $table->string('status');
            $table->unsignedBigInteger('leader')->nullable();
            $table->boolean('leader_is_playing');
            $table->unsignedDecimal('budget',6,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
