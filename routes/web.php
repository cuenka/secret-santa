<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout');
});

Route::get('room/entry/{id}/{token}', 'RoomController@entry')->name('room.entry');
//Route::get('room/entry', ['as' => 'room.entry', 'uses' => 'RoomController@entry']);
Route::post('room/check-entry', ['as' => 'room.check_entry', 'uses' => 'RoomController@checkEntry']);
Route::resource('room', 'RoomController');
Route::get('player/show', 'PlayerController@show')->name('player.show')->middleware('checkCookies');
