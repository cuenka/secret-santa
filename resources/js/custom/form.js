$(document).ready(function () {
    $("#app").on('click', '.clonable', function (e) {
        const id = 'player-' + Math.random().toString(36).substring(7);
        const html =`
 <div class="form-row player ${id} mt-2 mb-2">
        <div class="form-group col-md-5">
            <label for="miembro">Correo electronico</label>
            <input type="email" class="form-control" name="player[${id}][email]"
                   aria-describedby="miembroHelp" placeholder="correo electronico del jugador">
            <small id="emailHelp" class="form-text text-muted">correo electronico del participante del sorteo</small>

        </div>
        <div class="form-group col-md-5">
            <label for="miembro">Nombre</label>
            <input type="text" class="form-control" name="player[${id}][name]"
                   aria-describedby="miembroHelp" placeholder="nombre del jugador">
            <small id="emailHelp" class="form-text text-muted">Nombre del participante del sorteo</small>
        </div>
        <div class="form-group col-md-2 align-self-end text-right">
            <button type="button" class="btn btn-outline-danger removable"
                    data-removable_class=".form-row.player.${id}">
                <i class="fa fa-trash"></i> Borrar
            </button>
            <small id="emailHelp" class="form-text text-muted">Eliminar jugador</small>
        </div>
    </div>`;
        console.log('clone')
        $($(this).data('append_to_class')).append(html);
    });

    $("#app").on('click', '.removable', function () {
        console.log($(".form-row.player").length);
        const removable_class = $(this).data('removable_class');
        if ($(".form-row.player").length > 1) {
            $(removable_class).remove();
        }
    });
});
