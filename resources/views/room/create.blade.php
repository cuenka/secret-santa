@extends('layout')
@section('header')
    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container my-auto white p-4">
            <h1 class="h1 mb-1 text-left ">Crear Amigo invisible</h1>
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    <ul class="m-0 p-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{ Form::model($room,[ 'route' => 'room.store'] ) }}
            {{ Form::hidden('type', $type) }}

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="name_game">Nombre</label>
                    {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'name_game','placeholder'=> 'Nombre or titulo del juego']) }}
                    <small class="form-text text-muted">Nombre or titulo del juego</small>
                </div>
                <div class="form-group col-md-6">
                    <label for="date_of_game">Fecha</label>
                    {{ Form::date('date_of_game', null, ['class' => 'form-control']) }}
                    <small class="form-text text-muted">Cuando se reparten los regalos?</small>
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="miembro">Correo electronico</label>
                    {{ Form::email('email_leader', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'emailLeader','placeholder'=> 'nombre del lider']) }}
                    <small id="emailHelp" class="form-text text-muted">correo de la persona que organiza el
                        sorteo</small>

                </div>
                <div class="form-group col-md-6">
                    <label for="leader">Lider del grupo</label>
                    {{ Form::text('name_leader', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'nameLeader','placeholder'=> 'nombre del lider']) }}
                    <small class="form-text text-muted">Nombre de la persona que organiza el sorteo</small>
                </div>
            </div>
            <div class="form-group">
                <label for="leader">Lider participa en amigo invisible</label>
                <div>
                    <div class="form-check form-check-inline">
                        {{ Form::radio('leader_is_playing',1, true ,['class' => 'form-check-input','required' => 'required', 'id' => 'leader_is_playing_true']) }}
                        <label class="form-check-label" for="leader_is_playing_true">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        {{ Form::radio('leader_is_playing',0, false ,['class' => 'form-check-input', 'id' => 'leader_is_playing_false']) }}
                        <label class="form-check-label" for="leader_is_playing_false">No</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="budget">Presupuesto</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">No mas de...</div>
                    </div>
                    {{ Form::number('budget', null, ['required' => 'required', 'step' => '0.01', 'class' => 'form-control', 'id' => 'budget', 'placeholder' => 'presupuesto']) }}
                    <div class="input-group-append">
                        <div class="input-group-text">Euros</div>
                    </div>
                </div>
            </div>
            @include('partials.form_member')
            {{ Form::submit('Crear amigo invisible', ['class'=> 'btn btn-success']) }}

            {{ Form::close() }}
        </div>
    </header>
@stop
