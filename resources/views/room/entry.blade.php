@extends('layout')
@section('header')
    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container my-auto white p-4">
            <h1 class="h1 mb-1 text-left ">Unirte a amigo invisible</h1>

            {{ Form::model($room,[ 'route' => 'room.check_entry'] ) }}
            {{ Form::hidden('id', $id) }}
            {{ Form::hidden('room_id', $id) }}
            {{ Form::hidden('token', $token) }}

            <div class="form-group">
                <label for="name_game">Nombre</label>
                {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'name','placeholder'=> 'Nombre del participante']) }}
                <small class="form-text text-muted">Tu nombre para participar en el juego y que la gente sepa quien eres</small>
                @error('name')
                <div class="text-danger alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="code_game">Codigo</label>
                {{ Form::text('code_game', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'code_game','placeholder'=> 'Introduce el Codigo']) }}
                <small class="form-text text-muted">Codigo para participar en el amigo invisible, el creador del juego lo sabe</small>
                @error('code_game')
                <div class="text-danger alert-danger">{{ $message }}</div>
                @enderror
            </div>
            {{ Form::submit('Siguiente', ['class'=> 'btn btn-success']) }}

            {{ Form::close() }}
        </div>
    </header>
@stop
