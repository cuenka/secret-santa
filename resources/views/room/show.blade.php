@extends('layout')
@section('header')
    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container my-auto white p-4">
            <h1 class="h1 mb-1 text-left ">Amigo invisible creado</h1>
            <h3 class="h3">la contrasena es: <i class="text-info font-weight-bold">{{ $room->code_game }}</i></h3>
            <p>Comparte el siguiente enlace con la gente que quireras que participe en el juego: </p>
            <a href="{{ $entryUrl }}">Enlace</a>
            <p>Tambien puedes compartirlo por:</p>
            <a href="">Whatsapp</a>
            <a href="">Telegram</a>
            <a href="">Facebook</a>
            <a href="">instagram</a>
            <a href="">twitter</a>
            <a href="">Correo</a>
            <div>
                <h3>PLayers</h3>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Player</th>
                        <th scope="col">Buys to</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($assignments as $key => $assignment)
                        <tr>
                            <th scope="row">{{ ++$key }}</th>
                            <td>{{ $assignment['player']['name'] }}</td>
                            <td>{{ $assignment['buy_to']['name'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </header>
@stop
