@extends('layout')
@section('header')
    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container my-auto white p-4">
            <h1 class="mb-1 text-center ">Nuevo Amigo invisible</h1>
            <form method="POST" action="{{ route('room.store') }}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="POST">
                <div class="form-group">
                    <label for="leader">Lider del grupo</label>
                    <input type="text" class="form-control" id="leader" name="leader" aria-describedby="leaderHelp"
                           placeholder="nombre del lider">
                    <small id="emailHelp" class="form-text text-muted">Persona que organiza el sorteo</small>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary clonable" data-clone_class=".member"
                            data-append_to_class=".group-members">
                        <i class="fa fa-plus"></i> Anadir miembro
                    </button>
                </div>
                <div class="group-members">
                    @php
                        $member = "member-". Str::random(6);
                    @endphp
                    <div class="form-group member {{ $member }}">
                        <label for="miembro">Miembro</label>
                        <div class="form-inline">
                            <input type="text" class="form-control" id="miembro" name="member[]"
                                   aria-describedby="miembroHelp" placeholder="nombre del lider">
                            <button type="button" class="btn btn-outline-danger removable"
                                    data-removable_class=".form-group.{{ $member }}">
                                <i class="fa fa-trash"></i> Borrar
                            </button>
                        </div>
                        <small id="emailHelp" class="form-text text-muted">Persona que organiza el sorteo</small>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Submit</button>

            </form>

        </div>
    </header>
@stop
