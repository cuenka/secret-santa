@extends('layout')
@section('header')
    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container my-auto white p-4">
            <h1 class="h1 mb-1 text-left ">Amigo invisible</h1>
            {{ $player->getId() }}
            {{ $player->name }}
            {{ $player->room_id }}
        </div>
    </header>
@stop
