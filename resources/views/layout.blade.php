<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>@yield('title')</title>
</head>

<body>
<div id="app">
    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
        <i class="fa fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a class="js-scroll-trigger" href="#page-top">Start Bootstrap</a>
            </li>
            <li class="sidebar-nav-item">
                <a class="js-scroll-trigger" href="#page-top">Home</a>
            </li>
            <li class="sidebar-nav-item">
                <a class="js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="sidebar-nav-item">
                <a class="js-scroll-trigger" href="#services">Services</a>
            </li>
            <li class="sidebar-nav-item">
                <a class="js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>
            <li class="sidebar-nav-item">
                <a class="js-scroll-trigger" href="#contact">Contact</a>
            </li>
        </ul>
    </nav>

@section('header')
@section('errors')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
@show
    <!-- Header -->
        <header class="masthead d-flex">
            <div class="container text-center my-auto">
                <h1 class="mb-1">Amigo Invisible</h1>
                <h3 class="mb-5">
                    <em>Organiza to amigo invisible gratis y sin registro</em>
                </h3>
                <a class="btn btn-primary btn-xl js-scroll-trigger" href="{{ route('room.create', ['type'=> 'email']) }}">Crea amigo invisible con correos</a>
                <a class="btn btn-info btn-xl js-scroll-trigger" href="{{ route('room.create', ['type'=> 'anonimous']) }}">Crea amigo invisible</a>
                <a class="btn btn-secondary btn-xl js-scroll-trigger" href="{{ route('room.create') }}">Unete a un grupo</a>
            </div>
            <div class="overlay"></div>
        </header>
    <!-- Footer -->
        <footer class="footer text-center">
            <div class="container">
                <ul class="list-inline mb-5">
                    <li class="list-inline-item">
                        <a class="social-link rounded-circle text-white mr-3" href="#">
                            <i class="fa fa-android"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="social-link rounded-circle text-white mr-3" href="#">
                            <i class="fa fa-apple"></i>
                        </a>
                    </li>
                </ul>
                <p class="text-muted small mb-0">Copyright &copy; Your Website {{ date('Y') }}</p>
            </div>
        </footer>
    @show
</div>
</body>

</html>
