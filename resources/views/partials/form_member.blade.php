<div class="btn-group mb-4" role="group" aria-label="Basic example">
    <button type="button" class="btn btn-outline-primary clonable" data-clone_class=".player"
            data-append_to_class=".group-players">
        <i class="fa fa-plus"></i> Anadir jugador
    </button>
</div>

<div class="group-players">
    @php
        $player = "player-". Str::random(6);
    @endphp
    <div class="form-row player {{ $player }} mt-2 mb-2">
        <div class="form-group col-md-5">
            <label for="miembro">Correo electronico</label>
            <input type="email" class="form-control" name="player[{{ $player }}][email]"
                   aria-describedby="miembroHelp" placeholder="correo electronico del jugador">
            <small id="emailHelp" class="form-text text-muted">correo electronico del participante del sorteo</small>

        </div>
        <div class="form-group col-md-5">
            <label for="miembro">Nombre</label>
            <input type="text" class="form-control" name="player[{{ $player }}][name]"
                   aria-describedby="miembroHelp" placeholder="nombre del jugador">
            <small id="emailHelp" class="form-text text-muted">Nombre del participante del sorteo</small>
        </div>
        <div class="form-group col-md-2 align-self-end text-right">
            <button type="button" class="btn btn-outline-danger removable"
                    data-removable_class=".form-row.player.{{ $player }}">
                <i class="fa fa-trash"></i> Borrar
            </button>
            <small id="emailHelp" class="form-text text-muted">Eliminar jugador</small>
        </div>
    </div>
</div>
